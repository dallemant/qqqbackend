#/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Flask
from tinydb import TinyDB
from tinydb.storages import MemoryStorage



app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config')
app.config.from_pyfile('config.py')

if app.config['TINY_DB_FILE'] is None:
    db = TinyDB(storage=MemoryStorage)
else:
    db = TinyDB(app.config['TINY_DB_FILE'])

from qqq import api, views
