#/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import jsonify, request
from tinydb import  Query

from . import app, db


class TinydbHelper:
    def __init__(self):
        pass



class ModelHelper:

    @staticmethod
    def is_composant(data):

        for field in ['name','version','url','protocol']:
            if not field in data:
              return False,"field '{0}' is not in data".format(field)
            if len(data[field]) == 0:
                return False, "field '{0}' empty".format(field)
        return True,""

    @staticmethod
    def contains_table(database, name):
        tables = list(database.tables())
        return name in tables



@app.route('/api')
def api():
    toto = {"hello": "world", "year": 2017}
    return jsonify(toto)

@app.route('/api/register', methods=['PUT'])
def register_composant():
    data = request.get_json(silent=False)

    is_composant,msg = ModelHelper.is_composant(data)

    if is_composant is False:
        return jsonify({'register': False , 'error': msg})

    composant = db.table(data['name'])

    query = Query()


    if len(composant.search((query.name == data['name']) & (query.version == data['version']))) > 0:
        return jsonify({'register': False , 'error': 'composant {0} version {1} already registered'.format(data['name'],data['version'])})

    composant.insert(data)
    return jsonify({'register': True})

@app.route('/api/resolve/<composant>/<version>', methods=['GET'])
def resolve_composant(composant,version):

    if ModelHelper.contains_table(db, composant) is False:
        return jsonify({'error': 'backend doesnt know composant {0}  '.format(composant)})

    query = Query()
    table  = db.table(composant)

    if table.contains( (query.name == composant) & (query.version == version) ) is False:
        return jsonify({'error': 'composant {0} with version {1} is not registered in backend'.format(composant,version)})

    ret = table.get( (query.name == composant) & (query.version == version) )

    return jsonify( ret )
