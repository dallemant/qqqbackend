import unittest
from tinydb import TinyDB, Query
from tinydb.storages import MemoryStorage


class MyTestCase(unittest.TestCase):
    def test_add_table_and_retrive_it(self):
        db = TinyDB(storage=MemoryStorage)
        compo = db.table("libXXX")
        tables = list( db.tables())
        self.assertTrue("libXXX" in tables  )
        libXXX =  db.table("libXXX")
        self.assertTrue(libXXX ==  compo )


        id = compo.insert({'name':12})
        self.assertTrue(id == 1)

        query = Query()
        ret = compo.search(query.name == 12)

        self.assertTrue(len(ret) == 1)




if __name__ == '__main__':
    unittest.main()

