from flask_testing import TestCase
import unittest
import json

from qqq import app


class MyTest(TestCase):

    libXXX_description = {'name': 'libXXX',
                'version': "1.2.3",
                'url': 'svn://repo/libXXX/tags/1.2.3',
                'protocol':'subversion'}

    def create_app(self):
        # pass in test configuration
        app.testing = True
        return app

    @classmethod
    def setUpClass(self):
        print("test setup")

    def test_api_ping(self):
        response = self.client.get("/api")
        self.assertEqual(response.json, {"hello": "world", "year": 2017} )

    def test_register_valid_componant(self):
        response = self.client.put("/api/register", data=json.dumps(self.libXXX_description), content_type='application/json')
        self.assertEqual(response.json, {'register': True})


    def data_without_field(self, field):
        d = dict(self.libXXX_description)
        del d[field]
        return d

    def test_cannot_register_composant_with_missing_field(self):

        for field in ['name', 'version', 'url', 'protocol']:
            datas = self.data_without_field(field)
            response = self.client.put("/api/register", data=json.dumps(datas), content_type='application/json').json

            self.assertFalse( response["register"] )
            self.assertTrue(len(response["error"]) >0)


    def test_cannot_modify_component_url(self):

        comp = dict(self.libXXX_description)
        comp['name']='libB'

        response = self.client.put("/api/register", data=json.dumps(comp), content_type='application/json').json
        self.assertTrue( response["register"] )

        #modify url is not
        comp['url'] = 'svn://repo/other/tags/1.2.3'

        response = self.client.put("/api/register", data=json.dumps(comp), content_type='application/json').json
        self.assertFalse(response["register"])

    def test_retrieve_registered_componant(self):
        lib = {'name': 'libYYY',
                              'version': "1.2.3",
                              'url': 'svn://repo/libYYY/tags/1.2.3',
                              'protocol': 'subversion'}

        self.client.put("/api/register", data=json.dumps(lib), content_type='application/json')

        response = self.client.get("/api/resolve/{0}/{1}".format(lib['name'],lib['version']) , data=json.dumps(self.libXXX_description), content_type='application/json').json

        self.assertEqual( response, lib )

    def test_retrieve_componant_fail_if_not_registered(self):
        lib = {'name': 'libZZZ',
               'version': "1.2.3",
               'url': 'svn://repo/libYYY/tags/1.2.3',
               'protocol': 'subversion'}

        response = self.client.get("/api/resolve/{0}/{1}".format(lib['name'], lib['version']),
                                   data=json.dumps(self.libXXX_description), content_type='application/json').json

        self.assertTrue( len( response['error'] ) > 0 )



if __name__ == '__main__':
    unittest.main()